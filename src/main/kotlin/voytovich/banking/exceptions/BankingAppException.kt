package voytovich.banking.exceptions

import voytovich.banking.server.APIParameterKey


/**
 * A root for application's exceptions
 */
open class BankingAppException(message: String, cause: Throwable? = null) : Exception(message, cause)


class ParameterValidationException(parameter: APIParameterKey?, message: String) :
    BankingAppException("Invalid parameter${if (parameter == null) "" else ": $parameter"} - $message")


class EntityAlreadyExistsException(entityName: String):
    BankingAppException("$entityName already exists")
