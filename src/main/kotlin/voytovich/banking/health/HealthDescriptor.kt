package voytovich.banking.health

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import voytovich.banking.server.ApplicationDescriptor

private const val Ok = "OK"

object HealthDescriptor : ApplicationDescriptor {
    override fun routes(context: Route) {
        context.get("/health") {
            call.respondText(Ok, ContentType.Text.Plain, HttpStatusCode.OK)
        }
    }
}
