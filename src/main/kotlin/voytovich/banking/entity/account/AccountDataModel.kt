package voytovich.banking.entity.account

import java.util.concurrent.atomic.AtomicLong

val defaultCurrency = AccountCurrency.Euro

enum class AccountCurrency {
    Euro
}

class AccountDataModel(val currency: AccountCurrency) {
    private val atomicBalance = AtomicLong(0)

    val balance: Long
        get() = atomicBalance.get()

    fun add(value: Long): Boolean {
        return subtract(-value)
    }

    /**
     * @return [true] if the value correctly subtracted, if
     * account balance is insufficient [false] will be returned
     */
    fun subtract(value: Long): Boolean {
        var isSufficient = true

        atomicBalance.getAndUpdate { currentValue ->
            if (currentValue >= value) {
                currentValue - value
            } else {
                isSufficient = false
                currentValue
            }
        }

        return isSufficient
    }
}
