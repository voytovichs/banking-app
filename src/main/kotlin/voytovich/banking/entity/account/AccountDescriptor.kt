package voytovich.banking.entity.account

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.KtorExperimentalAPI
import voytovich.banking.exceptions.ParameterValidationException
import voytovich.banking.server.*

class AccountDescriptor(private val storage: AccountsStorage) : ApplicationDescriptor {
    @KtorExperimentalAPI
    override fun routes(context: Route) {
        context.route("/account") {
            get("/{login}") {
                val login = call.parameters.login
                val data = getAccount(login)?.toResponse()

                if (data == null) {
                    call.loginNotFoundResponse(login)
                } else {
                    call.respondWithData(data, HttpStatusCode.OK)
                }
            }

            post("/put/{login}") {
                val login = call.parameters.login
                val data = call.receiveRequest()
                val amount = data.amount
                val errorMessage = putMoney(login, amount)

                if (errorMessage == null) {
                    call.respond(HttpStatusCode.Accepted)
                } else {
                    call.respondText(errorMessage, ContentType.Text.Plain, HttpStatusCode.BadRequest)
                }
            }

            post("/transfer") {
                val data = call.receiveRequest()
                val sender = data.getRequired(AccountOperation.Sender)
                val beneficiary = data.getRequired(AccountOperation.Beneficiary)
                val amount = data.amount
                if (amount <= 0L) {
                    throw ParameterValidationException(AccountOperation.Amount, "Must be a positive number")
                }

                val errorMessage = transferMoney(sender, beneficiary, amount)
                call.sendAcceptedOrError(errorMessage)
            }
        }
    }

    private fun getAccount(login: String): AccountDataModel? = storage.getAccount(login)

    /**
     * @return null ff operation has succeded, otherwise a return message returned
     */
    private fun putMoney(login: String, amount: Long): String? {
        val account = getAccount(login) ?: return "Account is not found"
        if (!account.add(amount)) {
            return "Insufficient balance"
        }
        return null
    }

    /**
     * @return null ff operation has succeded, otherwise a return message returned
     */
    private fun transferMoney(sender: String, beneficiary: String, amount: Long): String? {
        val senderAccount = getAccount(sender) ?: return "Accounts with login $sender is not found"
        val beneficiaryAccount = getAccount(beneficiary) ?: return "Accounts with login $beneficiary is not found"

        if (!senderAccount.subtract(amount)) {
            return "Insufficient balance"
        }

        beneficiaryAccount.add(amount)

        return null
    }

    private val RequestParameters.amount: Long
        get() = this.getRequired(AccountOperation.Amount).toLongOrNull()
            ?: throw ParameterValidationException(AccountOperation.Amount, "Must be a number")

    private suspend fun ApplicationCall.sendAcceptedOrError(errorMessage: String?) {
        if (errorMessage == null) {
            respond(HttpStatusCode.Accepted)
        } else {
            respondText(errorMessage, ContentType.Text.Plain, HttpStatusCode.BadRequest)
        }
    }
}
