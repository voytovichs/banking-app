package voytovich.banking.entity.account

import voytovich.banking.server.APIParameterKey

enum class AccountParameters(override val key: String) : APIParameterKey {
    Balance("balance"), Currency("currency")
}

enum class AccountOperation(override val key: String) : APIParameterKey {
    Amount("amount"), Sender("sender"), Beneficiary("beneficiary")
}

fun AccountDataModel.toResponse() = mapOf(
    AccountParameters.Balance.key to balance.toString(),
    AccountParameters.Currency.key to currency.name
)
