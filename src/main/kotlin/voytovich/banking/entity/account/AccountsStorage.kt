package voytovich.banking.entity.account

import voytovich.banking.storage.KeyValueStorage

interface AccountsStorage {
    fun createAccount(login: String): AccountDataModel
    fun getAccount(login: String): AccountDataModel?
    fun deleteAccount(login: String): AccountDataModel?
}

class AccountsStorageImpl(private val kv: KeyValueStorage<String, AccountDataModel>) : AccountsStorage {
    override fun createAccount(login: String): AccountDataModel {
        val new = AccountDataModel(defaultCurrency)
        val prev = kv.putIfAbsent(login, new)
        if (prev != null) {
            throw IllegalStateException("Failed to create an account for '$login': the account with equal is already existsw")
        }
        return new
    }

    override fun getAccount(login: String): AccountDataModel? = kv[login]

    override fun deleteAccount(login: String): AccountDataModel? = kv.remove(login)
}
