package voytovich.banking.entity.holder

import voytovich.banking.exceptions.EntityAlreadyExistsException
import voytovich.banking.storage.KeyValueStorage

interface AccountHoldersStorage {
    fun create(login: String): AccountHolderDataModel

    fun getByLogin(login: String): AccountHolderDataModel?

    fun delete(login: String): AccountHolderDataModel?
}

class AccountHoldersStorageImpl(private val kv: KeyValueStorage<String, AccountHolderDataModel>) : AccountHoldersStorage {
    override fun create(login: String): AccountHolderDataModel {
        val newValue = AccountHolderDataModel(login)
        val value = kv.putIfAbsent(login, newValue)
        if (value != null) {
            throw EntityAlreadyExistsException("Account holder")
        }

        return newValue
    }

    override fun getByLogin(login: String): AccountHolderDataModel? = kv[login]

    override fun delete(login: String): AccountHolderDataModel? = kv.remove(login)
}
