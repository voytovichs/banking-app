package voytovich.banking.entity.holder

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.util.KtorExperimentalAPI
import voytovich.banking.entity.account.AccountsStorage
import voytovich.banking.server.*

class AccountHoldersDescriptor(
    private val storage: AccountHoldersStorage,
    private val accountStorage: AccountsStorage
) : ApplicationDescriptor {
    @KtorExperimentalAPI
    override fun routes(context: Route) {
        context.route("/account-holder") {
            post("new") {
                val requestData = call.receiveRequest()
                val login = LoginValidator.validate(LoginValidator.sanitize(requestData.getRequired(Identity.Login)))
                val new = newAccountHolder(login)
                call.respondWithData(new.toResponse(), HttpStatusCode.Created)
            }

            get("/{login}") {
                val login = call.parameters.login
                val data = getAccountByLogin(login)

                if (data == null) {
                    call.loginNotFoundResponse(login)
                } else {
                    call.respondWithData(data.toResponse(), HttpStatusCode.OK)
                }
            }

            delete("/{login}") {
                val login = call.parameters.login
                val deletedAccount = deleteAccountHolder(login)

                if (deletedAccount == null) {
                    call.loginNotFoundResponse(login)
                } else {
                    call.respond(HttpStatusCode.Accepted)
                }

                accountStorage.deleteAccount(login)
            }
        }
    }

    private fun newAccountHolder(login: String): AccountHolderDataModel {
        val newAccountHolder = storage.create(login)
        accountStorage.createAccount(login)
        return newAccountHolder
    }

    @KtorExperimentalAPI
    private fun getAccountByLogin(login: String): AccountHolderDataModel? = storage.getByLogin(login)


    @KtorExperimentalAPI
    private fun deleteAccountHolder(login: String): AccountHolderDataModel? = storage.delete(login)
}
