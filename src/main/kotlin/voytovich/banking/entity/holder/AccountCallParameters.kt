package voytovich.banking.entity.holder

import voytovich.banking.exceptions.ParameterValidationException
import voytovich.banking.server.Identity

object LoginValidator {
    private const val minLength = 5
    private const val maxLength = 60
    private val allowedCharacters = Regex("[A-Za-z0-9_-]+")


    fun sanitize(login: String): String = login.trim().toLowerCase()

    fun validate(login: String): String {
        if (login.length < minLength) {
            throw ParameterValidationException(Identity.Login, "must be at least $minLength characters long")
        }

        if (login.length > maxLength) {
            throw ParameterValidationException(Identity.Login, "must be at most $maxLength characters long")
        }

        if (!allowedCharacters.matches(login)) {
            throw ParameterValidationException(
                Identity.Login,
                "contains restricted characters: valid values are latin letters, digits, '_' and '-'"
            )
        }
        return login
    }
}

fun AccountHolderDataModel.toResponse() = mapOf(Identity.Login.key to login)

