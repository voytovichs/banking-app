package voytovich.banking.entity.holder

data class AccountHolderDataModel(
    val login: String
)
