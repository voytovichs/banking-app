package voytovich.banking.storage

import java.util.concurrent.ConcurrentMap

typealias KeyValueStorage<K, V> = ConcurrentMap<K, V>

