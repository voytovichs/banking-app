package voytovich.banking.server

import io.ktor.routing.Route

interface ApplicationDescriptor {
    fun routes(context: Route)
}
