package voytovich.banking.server

import voytovich.banking.exceptions.BankingAppException


private const val Delimiter = ";"
private const val PairSeparator = ":"

class ParametersParsingException(message: String) : BankingAppException(message)

/**
 * Parameters are key-value pairs, separated by [Delimeter].
 * Each pair key is separated from the value by a [PairSeparator].
 * Value absence is tread as null.
 *
 * key1[PairSeparator]value1[Delimiter]key2[PairSeparator]value2
 */

object RequestParametersDecoder {
    fun decode(value: String): Map<String, String?> = value.split(Delimiter).asSequence().mapNotNull {
        val keyAndValue = it.split(PairSeparator, limit = 2)
        when {
            value.isBlank() -> null
            keyAndValue.size == 1 -> keyAndValue.single() to null
            else -> keyAndValue[0] to keyAndValue[1]
        }
    }.toMap()
}


object RequestParametersEncoder {
    fun encode(parameters: Map<String, String>): String = buildString {
        var first = true
        parameters.forEach { k, v ->
            if (!first) {
                append(Delimiter)
            } else {
                first = false
            }

            append(k)
            append(PairSeparator)
            append(v)
        }
    }
}
