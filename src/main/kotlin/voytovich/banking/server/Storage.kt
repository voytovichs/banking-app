package voytovich.banking.server

import voytovich.banking.entity.account.AccountDataModel
import voytovich.banking.entity.account.AccountsStorageImpl
import voytovich.banking.entity.holder.AccountHolderDataModel
import voytovich.banking.entity.holder.AccountHoldersStorageImpl
import java.util.concurrent.ConcurrentHashMap

class EntityStorage {
    val accountHolders = AccountHoldersStorageImpl(ConcurrentHashMap<String, AccountHolderDataModel>())
    val accounts = AccountsStorageImpl(ConcurrentHashMap<String, AccountDataModel>())
}
