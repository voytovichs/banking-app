package voytovich.banking.server

import io.ktor.application.ApplicationCall
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receiveText
import io.ktor.response.respondText
import io.ktor.util.KtorExperimentalAPI
import voytovich.banking.exceptions.BankingAppException
import voytovich.banking.exceptions.ParameterValidationException
import java.net.URLDecoder

interface APIParameterKey {
    val key: String
}

enum class Identity(override val key: String) : APIParameterKey {
    Login("login")
}

fun urlDecode(value: String) = URLDecoder.decode(value, Charsets.UTF_8.name())!!

class RequestParameterException(message: String, cause: Throwable? = null) : BankingAppException(message, cause)

class RequestParameters(private val parameters: Map<String, String?>) {
    fun getRequired(key: APIParameterKey): String {
        return getOptional(key) ?: throw RequestParameterException("Required parameter '$key' is not present")
    }

    fun getOptional(key: APIParameterKey): String? {
        return parameters[key.key]
    }
}


// Requests

suspend fun ApplicationCall.receiveRequest(): RequestParameters {
    val text = try {
        this.receiveText()
    } catch (t: Throwable) {
        throw RequestParameterException("Failed to read the request", t)
    }

    return RequestParameters(RequestParametersDecoder.decode(text))
}

// Responses

suspend fun ApplicationCall.respondWithData(response: Map<String, String>, status: HttpStatusCode = HttpStatusCode.OK) {
    this.respondText(RequestParametersEncoder.encode(response), ContentType.Text.Plain, status)
}

@KtorExperimentalAPI
val Parameters.login
    get() = this["login"]?.let { urlDecode(it) } ?: throwRequiredParameterIsMissingException(Identity.Login)

suspend fun ApplicationCall.loginNotFoundResponse(login: String) {
    this.respondText(
        "Login '$login' doesn't belong to any existing account",
        ContentType.Text.Plain,
        HttpStatusCode.NotFound
    )
}


fun throwRequiredParameterIsMissingException(parameter: APIParameterKey): Nothing = throw ParameterValidationException(
    parameter, "required parameter is missing"
)
