package voytovich.banking.server

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.routing.Routing
import voytovich.banking.routes.applicationStatusPages
import voytovich.banking.routes.routes

fun Application.main() {
    val storage = EntityStorage()

    install(DefaultHeaders)

    install(StatusPages) {
        applicationStatusPages()
    }

    install(CallLogging)

    install(Routing) {
        routes(storage)
    }
}
