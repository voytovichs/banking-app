package voytovich.banking.routes

import io.ktor.application.call
import io.ktor.features.StatusPages
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.TextContent
import io.ktor.http.withCharset
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import voytovich.banking.entity.account.AccountDescriptor
import voytovich.banking.entity.holder.AccountHoldersDescriptor
import voytovich.banking.exceptions.EntityAlreadyExistsException
import voytovich.banking.exceptions.ParameterValidationException
import voytovich.banking.health.HealthDescriptor
import voytovich.banking.server.ApplicationDescriptor
import voytovich.banking.server.EntityStorage


fun Route.routes(storage: EntityStorage) {
    registerDescriptor(HealthDescriptor)
    registerDescriptor(AccountHoldersDescriptor(storage.accountHolders, storage.accounts))
    registerDescriptor(AccountDescriptor(storage.accounts))
}

private fun Route.registerDescriptor(descriptor: ApplicationDescriptor) {
    descriptor.routes(this)
}

fun StatusPages.Configuration.applicationStatusPages() {
    status(HttpStatusCode.NotFound) { s ->
        call.respond(TextContent("${s.value} ${s.description}", ContentType.Text.Plain.withCharset(Charsets.UTF_8), s))
    }

    exception<Throwable> { t ->
        val message = t.message ?: "Unknown internal server error ($t)"
        call.respondText(message, ContentType.Text.Plain, HttpStatusCode.InternalServerError)
        throw t
    }

    exception<ParameterValidationException> { e ->
        val message = e.message ?: "Missing message"
        call.respondText(message, ContentType.Text.Plain, HttpStatusCode.BadRequest)
    }

    exception<EntityAlreadyExistsException> { e ->
        val message = e.message ?: "Missing message"
        call.respondText(message, ContentType.Text.Plain, HttpStatusCode.BadRequest)
    }
}
