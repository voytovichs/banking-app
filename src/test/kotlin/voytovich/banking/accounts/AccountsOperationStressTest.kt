package voytovich.banking.accounts

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import voytovich.banking.app.client.Client
import voytovich.banking.app.server.withServer
import voytovich.banking.app.util.assertBalance
import voytovich.banking.server.Identity

class AccountsOperationStressTest {
    @Test
    fun `100 parallel transactions`() = withServer { client ->
        val logins = client.createOneHundredAccounts()
        logins.forEach { client.put(it, 100L) }

        val jobs = logins.map { GlobalScope.launch { client.sendMoneyToAccounts(it, logins, 1) } }
        jobs.forEach { it.join() }

        logins.forEach { client.assertBalance(it, 100) }
    }

    private suspend fun Client.createOneHundredAccounts(): List<String> =
        (0..99).map {
            createAccountHolder("test-account-$it").contentToMap().get(Identity.Login.key).checkPresent("login")
        }

    private suspend fun Client.sendMoneyToAccounts(sender: String, accounts: List<String>, amount: Long) {
        for (account in accounts) {
            if (sender != account) {
                transfer(sender, account, amount)
            }
        }
    }

    private fun <T> T?.checkPresent(parameterName: String): T =
        if (this == null) throw NullPointerException("$parameterName is null") else this
}

