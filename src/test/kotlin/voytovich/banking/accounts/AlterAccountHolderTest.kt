package voytovich.banking.accounts

import io.ktor.http.HttpStatusCode
import org.junit.Assert
import org.junit.Test
import voytovich.banking.app.client.Client
import voytovich.banking.app.server.withServer
import voytovich.banking.app.util.assertCodeEquals
import voytovich.banking.app.util.testLogin

class AlterAccountHolderTest {
    @Test
    fun `create account`() = withServer { client ->
        val createResponse = client.createAccountHolder(testLogin)
        Assert.assertEquals(HttpStatusCode.Created.value, createResponse.statusCode)

        val getResponse = client.getAccountHolder(testLogin)
        getResponse.assertCodeEquals(HttpStatusCode.OK)

        Assert.assertEquals(mapOf("login" to testLogin), getResponse.contentToMap())
    }

    @Test
    fun `login restrictions`() = withServer { client ->
        // illegal characters
        val l1 = "привет"
        val r1 = client.createAccountHolder(l1)
        r1.assertCodeEquals(HttpStatusCode.BadRequest)
        client.assertNotExist(l1)

        // short login
        val l2 = "srt"
        val r2 = client.createAccountHolder(l2)
        r2.assertCodeEquals(HttpStatusCode.BadRequest)
        client.assertNotExist(l2)

        // long login
        val l3 = "long".repeat(20)
        val r3 = client.createAccountHolder(l3)
        r3.assertCodeEquals(HttpStatusCode.BadRequest)
        client.assertNotExist(l3)
    }

    @Test
    fun `create conflicting account`() = withServer { client ->
        client.createAccountHolder(testLogin)
        val secondResponse = client.createAccountHolder(testLogin)
        secondResponse.assertCodeEquals(HttpStatusCode.BadRequest)
    }

    @Test
    fun `delete account`() = withServer { client ->
        client.createAccountHolder(testLogin)
        val deleteResponse = client.deleteAccountHolder(testLogin)
        deleteResponse.assertCodeEquals(HttpStatusCode.Accepted)
        client.assertNotExist(testLogin)
    }

    private suspend fun Client.assertNotExist(login: String) {
        val response = getAccountHolder(login)
        Assert.assertEquals(HttpStatusCode.NotFound.value, response.statusCode)
    }
}
