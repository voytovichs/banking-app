package voytovich.banking.accounts

import io.ktor.http.HttpStatusCode
import org.junit.Assert
import org.junit.Test
import voytovich.banking.app.server.withServer
import voytovich.banking.app.util.assertBalance
import voytovich.banking.app.util.assertCodeEquals
import voytovich.banking.app.util.testLogin
import voytovich.banking.app.util.testLogin2
import voytovich.banking.entity.account.AccountParameters

class AccountOperationTest {
    @Test
    fun `account is created for a new account holder`() = withServer { client ->
        client.createAccountHolder(testLogin)
        val r = client.getAccount(testLogin)
        r.assertCodeEquals(HttpStatusCode.OK)
        r.contentToMap().containsKey(AccountParameters.Balance.key)
    }


    @Test
    fun `default account balance`() = withServer { client ->
        client.createAccountHolder(testLogin)
        val r = client.getAccount(testLogin)
        Assert.assertEquals("0", r.contentToMap()[AccountParameters.Balance.key])
    }

    @Test
    fun `put money on the account`() = withServer { client ->
        client.createAccountHolder(testLogin)
        val r = client.put(testLogin, 47L)
        r.assertCodeEquals(HttpStatusCode.Accepted)
        client.assertBalance(testLogin, 47L)
    }

    @Test
    fun `take money from the account`() = withServer { client ->
        client.createAccountHolder(testLogin)
        client.put(testLogin, 100L)
        val r = client.put(testLogin, -53L)
        r.assertCodeEquals(HttpStatusCode.Accepted)
        client.assertBalance(testLogin, 47L)
    }

    @Test
    fun `take money from the account with insuffucien balance`() = withServer { client ->
        client.createAccountHolder(testLogin)
        client.put(testLogin, 100L)
        val r = client.put(testLogin, -101L)
        r.assertCodeEquals(HttpStatusCode.BadRequest)
        client.assertBalance(testLogin, 100L)
    }

    @Test
    fun `transfer money`() = withServer { client ->
        client.createAccountHolder(testLogin)
        client.createAccountHolder(testLogin2)
        client.put(testLogin, 100L)
        client.put(testLogin2, 100L)
        val r = client.transfer(testLogin, testLogin2, 53L)
        r.assertCodeEquals(HttpStatusCode.Accepted)
        client.assertBalance(testLogin, 47L)
        client.assertBalance(testLogin2, 153L)
    }

    @Test
    fun `iterated money transfer`() = withServer { client ->
        client.createAccountHolder(testLogin)
        client.put(testLogin, 100L)
        client.transfer(testLogin, testLogin, 47L)
        client.assertBalance(testLogin, 100L)
    }

    @Test
    fun `transfer money insufficient account balance`() = withServer { client ->
        client.createAccountHolder(testLogin)
        client.createAccountHolder(testLogin2)
        client.put(testLogin, 100L)
        client.put(testLogin2, 100L)
        val r = client.transfer(testLogin, testLogin2, 153L)
        r.assertCodeEquals(HttpStatusCode.BadRequest)
        client.assertBalance(testLogin, 100L)
        client.assertBalance(testLogin2, 100L)
    }
}
