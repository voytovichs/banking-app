package voytovich.banking.health

import io.ktor.http.HttpStatusCode
import org.junit.Assert
import org.junit.Test
import voytovich.banking.app.server.withServer

class HealthTest {
    @Test
    fun `test health response`() = withServer { client ->
        val response = client.health()
        Assert.assertEquals("OK", response.rawContent)
        Assert.assertEquals(HttpStatusCode.OK.value, response.statusCode)
    }
}
