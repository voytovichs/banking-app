package voytovich.banking.app.server

import io.ktor.server.engine.commandLineEnvironment
import io.ktor.server.netty.NettyApplicationEngine
import kotlinx.coroutines.runBlocking
import voytovich.banking.app.client.Client
import voytovich.banking.app.client.TestApplicationClient
import java.util.concurrent.TimeUnit

private var tc: Int = 0
private const val fixedPort = 8085

fun <T> withServer(withServer: suspend (Client) -> T) {
    val applicationPort = fixedPort + (tc++ % 20)

    val serverEnvironment = commandLineEnvironment(
        arrayOf("-port=$applicationPort")
    )
    val s = NettyApplicationEngine(serverEnvironment)
    s.start()
    try {
        runBlocking {
            TestApplicationClient(applicationPort).use { withServer(it) }
        }
    } finally {
        s.stop(100, 100, TimeUnit.MILLISECONDS)
    }
}
