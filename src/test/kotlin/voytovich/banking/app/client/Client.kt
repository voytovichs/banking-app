package voytovich.banking.app.client

interface Client {
    suspend fun health(): ServerResponse

    suspend fun createAccountHolder(login: String): ServerResponse
    suspend fun getAccountHolder(login: String): ServerResponse
    suspend fun deleteAccountHolder(login: String): ServerResponse

    suspend fun getAccount(login: String): ServerResponse
    suspend fun put(login: String, amount: Long): ServerResponse
    suspend fun transfer(sender: String, beneficiary: String, amount: Long): ServerResponse
}
