package voytovich.banking.app.client

import voytovich.banking.server.RequestParametersDecoder

data class ServerResponse(
    val rawContent: String,
    val contentType: String?,
    val statusCode: Int,
    val headers: Map<String, List<String>>
) {
    fun contentToMap(): Map<String, String?> = RequestParametersDecoder.decode(rawContent)
}
