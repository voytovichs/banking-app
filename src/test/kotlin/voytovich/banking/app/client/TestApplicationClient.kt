package voytovich.banking.app.client

import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.http.contentType
import io.ktor.util.toMap
import voytovich.banking.entity.account.AccountOperation
import voytovich.banking.server.RequestParametersEncoder
import java.io.Closeable
import java.util.concurrent.TimeUnit


class TestApplicationClient(private val applicationPort: Int) : Client, Closeable {
    private val client = HttpClient(OkHttp) {
        engine {
            // https://square.github.io/okhttp/3.x/okhttp/okhttp3/OkHttpClient.Builder.html
            config {
                followRedirects(true)
                readTimeout(3000, TimeUnit.MILLISECONDS)
                connectTimeout(3000, TimeUnit.MILLISECONDS)
            }
        }

    }

    // Health
    private val HealtPath = "health"

    override suspend fun health() =
        client.get<HttpResponse>(port = applicationPort, path = "/$HealtPath").toServerResponse()


    // Account holders
    private val AccountHolderPath = "account-holder"

    override suspend fun createAccountHolder(login: String) =
        client.post<HttpResponse>(port = applicationPort, path = "/$AccountHolderPath/new") {
            body = RequestParametersEncoder.encode(mapOf("login" to login))
        }.toServerResponse()


    override suspend fun getAccountHolder(login: String) =
        client.get<HttpResponse>(port = applicationPort, path = "/$AccountHolderPath/$login").toServerResponse()

    override suspend fun deleteAccountHolder(login: String) =
        client.delete<HttpResponse>(port = applicationPort, path = "/$AccountHolderPath/$login").toServerResponse()


    // Accounts
    private val AccountPath = "account"

    override suspend fun getAccount(login: String) =
        client.get<HttpResponse>(port = applicationPort, path = "/$AccountPath/$login").toServerResponse()

    override suspend fun put(login: String, amount: Long): ServerResponse =
        client.post<HttpResponse>(port = applicationPort, path = "/$AccountPath/put/$login") {
            body = RequestParametersEncoder.encode(mapOf(AccountOperation.Amount.key to amount.toString()))
        }.toServerResponse()

    override suspend fun transfer(sender: String, beneficiary: String, amount: Long) =
        client.post<HttpResponse>(port = applicationPort, path = "/$AccountPath/transfer") {
            body = RequestParametersEncoder.encode(
                mapOf(
                    AccountOperation.Sender.key to sender,
                    AccountOperation.Beneficiary.key to beneficiary,
                    AccountOperation.Amount.key to amount.toString()
                )
            )
        }.toServerResponse()


    override fun close() {
        client.close()
    }
}


private suspend fun HttpResponse.toServerResponse() = ServerResponse(
    rawContent = readText(Charsets.UTF_8),
    contentType = contentType()?.contentType,
    statusCode = status.value,
    headers = headers.toMap()
)

