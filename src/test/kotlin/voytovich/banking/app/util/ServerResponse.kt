package voytovich.banking.app.util

import io.ktor.http.HttpStatusCode
import org.junit.Assert
import voytovich.banking.app.client.Client
import voytovich.banking.app.client.ServerResponse
import voytovich.banking.entity.account.AccountParameters

fun ServerResponse.assertCodeEquals(expected: HttpStatusCode) {
    Assert.assertEquals("Response:$rawContent", expected.value, this.statusCode)
}

suspend fun Client.assertBalance(login: String, expected: Long) =
    Assert.assertEquals(expected.toString(), getAccount(login).contentToMap().get(AccountParameters.Balance.key))
