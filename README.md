# Banking application

## General Description
There are two entites **Account Holder** and **Account**, each account holder has exactly one account and vice versa (couldn't resist leaving a room for having multiple accounts for an account holder)

Accounts's lifetime is the same with account holder's.

I'd suggest to start exploring actual capabilities from the file `voytovich.banking.app.client.Client.kt` located in the test module.

Request are landing at implementations of `voytovich.banking.server.ApplicationDescriptor.kt`, that'd be a great place to start to explore the server
##Dependencies
1. **Netty** - servlet container
2. **Ktor** - http framework
3. **OkHttp** - http client
4. **JUnit** - test framework

 
